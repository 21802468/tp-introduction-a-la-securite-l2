#include <stdio.h>
#include <stdlib.h>


struct cell_t {
  void* val;
  unsigned long int id;
  struct cell_t* next;
};

typedef struct cell_t* list_t;

list_t list_empty(){
  return NULL;
}

int list_is_empty(list_t list){
  return list == NULL;
}

list_t list_push(list_t l, void* x){
  /*
  list_t celule = malloc(sizeof(struct cell_t));
  celule -> val = x;
  
  if (list_is_empty(l) != 1){
    celule -> id = l -> id + 1;
    celule -> next = l;
  }
  else{
    celule -> id = 1;
    celule -> next = NULL;
  }

  return celule;
  */

  list_t celule = malloc(sizeof(struct cell_t));
  celule -> val = x;
  celule -> next = l;
  
  if (list_is_empty(l)){
    celule -> id = 1;
  }
  else{
    celule -> id = l-> id + 1;
  }
  return celule;

}

list_t list_tail(list_t l){
  if(list_is_empty(l)){
    return NULL;
  }
  return l->next;


}

void* list_pop(list_t* l){ 
  void* res = (*l) -> val;
  list_t queue = (*l)->next;
  free(*l);
  *l = queue;
  return res;
}

void* list_top(list_t l){
  if(list_is_empty(l)){
    return NULL;
  }
  return l->val;
}

void list_destroy(list_t l, void (*free_void)(void*)){
  while(list_is_empty(l)){
    free_void(list_pop(&l));
  }


}

// return the found element or NULL
void* list_in(list_t l, void* x, int (*eq)(void*, void*)){
  /*list_t tmp = l;
  while(tmp != NULL){
    if (eq(tmp->val,x)){
      return tmp->val;
    }
    tmp = tmp -> next;
  }
  return NULL;*/
  


  for(list_t curr = l; !list_is_empty(curr); curr = curr->next)
    if(eq(curr->val,x))
      return curr->val;
  return NULL;
  



}

unsigned long int list_len(list_t l){
  if(list_empty(l)){
    return 0;
  }
  return l->id;
}

